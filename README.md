# String Formatter for C++.
This is a formatter very simillar to the one used in C# for strings in C++.

## Installing
Simply drag the StringFormatter.h file into your project and include it where you want to use it.

### Examples

Instead of writing:

```cpp
#include <iostream>

std::string name = "exampleName";
std::string city = "exampleCity";

std::string name = "Hi my name is " + name + " and I live in " + city;

//Will result in: "Hi my name is exampleName and I live in exampleCity"
```

You can write the following using this library:
```cpp
#include <iostream>
#include "StringFormatter.h"

std::string name = "exampleName";
std::string city = "exampleCity";
std::string name = stringFormatter::format("Hi my name is {0} and I live in {1}", name, city);

//Will result in: "Hi my name is exampleName and I live in exampleCity"
```

Furthermore, just like C# you can use multiple locations for a single argument:

```cpp
#include <iostream>
#include "StringFormatter.h"

std::string name = "exampleName"
std::string city = "exampleCity"
std::string name = stringFormatter::format("Hi my name is {0} and I live in {1} and my favorite city is {1}", name, city); 

//Will result in: "Hi my name is exampleName and I live in exampleCity and my favorite city is exampleCity"
```

You can use any type of variable you want as long as it supports a to_string function.