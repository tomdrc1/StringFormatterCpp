#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

// Use this to unpack the arguments into the vector
// Has to be outside of the class so it will count as an overload function to the regular std::to_string function.
inline std::string to_string(const std::string& s) { return s; }

/*
 *  Very basic StringFormatter written by tomdrc1.
 *  Will use C# format style formatting ({0} -> arg 0, {2} -> arg 2 etc)
 *  This basic StringFormatter was wirrten to make my "sqlite3 using" life much much easier.
 *  Feel free to use this everywhere you see fit.
 */
class stringFormatter
{
public:
	/**
	 * Will format the string you enter it with the list of args you give it
	 * 
	 * @param stringToFormat The string we want to format with the arguments
	 * @param args The arguments we want to format into the string.
	 *
	 * @return The formatted string with the arguments inside it.
	 */
	template<typename ... Args>
	static std::string format(std::string stringToFormat, const Args& ... args)
	{
		std::string result = "";
		std::vector<std::string> argsList;

		//Telling the function to first use my custom made to_string. If it's not a string use the regular std::to_string.
		using ::to_string;
		using std::to_string;
		int unpack[]{ 0, (argsList.push_back(to_string(args)), 0)... };
		static_cast<void>(unpack);
		
		int i = 0;
		
		for (i = 0; i < argsList.size(); ++i)
		{
			std::string tempArgTemplate = "{" + std::to_string(i) + "}";

			int pos = stringToFormat.find(tempArgTemplate);
			
			//in case there are more than 1 use to that format
			while (pos != std::string::npos)
			{
				stringToFormat.replace(pos, tempArgTemplate.size(), argsList[i]);
				pos = stringToFormat.find(tempArgTemplate);
			}
		}
		
		return stringToFormat;
	}
};